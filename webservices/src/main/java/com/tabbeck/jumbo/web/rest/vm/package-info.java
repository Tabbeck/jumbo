/**
 * View Models used by Spring MVC REST controllers.
 */
package com.tabbeck.jumbo.web.rest.vm;

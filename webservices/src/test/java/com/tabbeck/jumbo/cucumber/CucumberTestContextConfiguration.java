package com.tabbeck.jumbo.cucumber;

import com.tabbeck.jumbo.JumboApp;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = JumboApp.class)
@WebAppConfiguration
public class CucumberTestContextConfiguration {}
